const rp = require('request-promise');
const cheerio = require('cheerio');
const options = {
  uri: `http://edtmobilite.wigorservices.net/WebPsDyn.aspx?Action=posETUDSEM&serverid=h&Tel=pierre.strobel&date=09/17/2018%208:00`,
  transform: function (body) {
    return cheerio.load(body);
  }
};

rp(options)
  .then(($) => {
    //get jours de la semaine 
    const jours = [];
    $('.TCJour').each(function (i, elem) {
      jours.push($(this).text());
    });

    let cours = []
    let horaires = []
    let salles = []
    let profs = []
    let matieres = []
    //Recupère chaque cours uniquement
    $('table').filter('.TCase').each(function (i, elem) {

      cours.push({
        'id': i,

      })

    })
    cours = cours.slice(5, cours.length)
    //Recupère chaque Horaire pour chaque cours
    $('.TChdeb').each(function (i, elem) {
      let hor = $(this).text().split("-")
      horaires.push({
        'id': i + 5,
        'horaire_debut': hor[0].replace(' ', ''),
        'horaire_fin': hor[1].replace(' ', '')
      })
    })
    //Recupère chaque Salle
    $('.TCSalle').each(function (i, elem) {
      salles.push({
        'id': i + 5,
        'salle': $(this).text()
      })

    })
    $('.TCProf').each(function (i, elem) {
      profs.push({
        'id': i + 5,
        'prof': $(this).text()
      })
    })
    $('td').filter('.TCase').each(function (i, elem) {
      matieres.push({
        'id': i + 5,
        'matiere': $(this).text()
      })
    })

    //Parsing final des cours
    for (let index = 0; index < cours.length; index++) {
      cours[index].horaire_debut = horaires[index].horaire_debut
      cours[index].horaire_fin = horaires[index].horaire_fin
      cours[index].salle = salles[index].salle
      cours[index].prof = profs[index].prof
      cours[index].matiere = matieres[index].matiere
    }
    //Assignation des cours à des jours de la semaine
    //console.log(jours)
    let jcourant = 0
    cours.forEach(function (item, index) {

      item.jour = jours[jcourant]

      if (index > 0 && cours[index - 1].horaire_fin > item.horaire_debut) {

        item.jour = jours[jcourant + 1]
        jcourant++
      }

    });
    console.log(cours)



  })
  .catch((err) => {
    console.log(err);
  });